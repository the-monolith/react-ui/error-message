import { component, React, Style } from 'local/react/component'

export const defaultErrorStyle: Style = {
  background: '#a00',
  color: '#fff',
  padding: '0 0.25em'
}

export const defaultErrorMessage = 'Error'

export const ErrorMessage = component
  .props<{
    style?: Style
    message?: string
  }>({
    message: defaultErrorMessage,
    style: defaultErrorStyle
  })
  .render(({ message, style }) => <div style={style}>{message}</div>)
